package ru.ivanov.tm;

import ru.ivanov.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        System.out.println("WELCOME TO TASK MANAGER");
        if (parseArgs(args)) System.exit(0);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseArg(command);
        }
    }

    public static void displayHelp() {
        System.out.println("version - Display program version.");
        System.out.println("about - Display developer info.");
        System.out.println("help - Display list of terminal commands.");
    }

    private static void displayVersion() {
        System.out.println("1.0.0");
    }

    private static void displayAbout() {
        System.out.println("Max Ivanov");
    }

    private static void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case TerminalConst.CMD_ABOUT:
                displayAbout();
                break;
            case TerminalConst.CMD_HELP:
                displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                displayVersion();
                break;
            case TerminalConst.CMD_EXIT:
                exit();
                break;
            default:
                showIncorrectCommand();
        }
    }

    private static boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private static int exit() {
        System.exit(0);
        return 0;
    }

    private static void showIncorrectCommand() {
        System.out.println("Error! Command not found...");
    }

}

